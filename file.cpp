#include "file.h"
#include "ui_file.h"

File::File(QString &path, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::File)
{
    ui->setupUi(this);
    this->setWindowTitle(QFileInfo(path).fileName());

    QFile file(path);
    if(file.open(QIODevice::ReadOnly)){
        QString text = file.readAll();
        ui->textEdit->setText(text);
    }
    else {
        Alert a("NIe udało się utworzyć pliku", this);
        a.exec();
    }
}

File::~File()
{
    delete ui;
}
