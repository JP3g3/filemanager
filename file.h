#ifndef FILE_H
#define FILE_H

#include <QDialog>
#include <QFile>
#include <QFileInfo>
#include "alert.h"

namespace Ui {
class File;
}

class File : public QDialog
{
    Q_OBJECT

public:
    explicit File(QString &path, QWidget *parent = 0);
    ~File();

private:
    Ui::File *ui;
};

#endif // FILE_H
