#ifndef LOGIN_H
#define LOGIN_H

#include <QDialog>

namespace Ui {
class login;
}

class login : public QDialog
{
    Q_OBJECT

public:
    explicit login(QWidget *parent = 0);
    QString getHost();
    QString getLogin();
    QString getPassword();
    ~login();

private slots:
    void on_buttonBox_accepted();

private:
    Ui::login *ui;
    QString host;
    QString loginName;
    QString password;
};

#endif // LOGIN_H
