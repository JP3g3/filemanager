#include "alert.h"
#include "ui_alert.h"

Alert::Alert(QString text, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Alert)
{
    ui->setupUi(this);
    ui->label->setText(text);
}

Alert::~Alert()
{
    delete ui;
}

void Alert::on_pb_ok_clicked()
{
    Alert::close();
}
