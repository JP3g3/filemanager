#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include <QFileSystemModel>
#include <QObject>
#include <QStack>
#include <QFile>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QTimer>
#include <QUrl>
#include <Windows.h>
#include "alert.h"
#include "dialog.h"
#include "login.h"
#include "file.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_lv_left_doubleClicked(const QModelIndex &index);
    void on_lv_right_doubleClicked(const QModelIndex &index);
    void on_lv_left_clicked(const QModelIndex &index);
    void on_lv_right_clicked(const QModelIndex &index);
    void on_pb_arrowup_left_clicked();
    void on_pb_arrowup_right_clicked();
    void on_pb_move_right_clicked();
    void on_pb_move_left_clicked();
    void on_pb_copy_right_clicked();
    void on_pb_copy_left_clicked();
    void openFile();
    void exit();
    void changeName();
    void hideFile();
    void unhideFile();
    void deleteFileOrFolder();
    void createFile();
    void createFolder();
    void connectToServer();
    void disconnectServer();

private:
    Ui::MainWindow *ui;
    QFileSystemModel *fileModelLeft;
    QFileSystemModel *fileModelRight;
    QStack<QString> stackOfPathsLeft;
    QStack<QString> stackOfPathsRight;
    QString pathLeft;
    QString pathRight;
    QString hostName;
    QString userName;
    QString password;

    void copyFileAndFolder(QString &src, QString &dst);
    bool copyFolder(const QString &src, const QString &dst);
    void moveFileAndFolder(QString &src, QString &dst);
    void deleteFile(QString &src);
    void deleteFolder(QString &src);
    void renameFile(QString &src, QString &newName);
    void renameFolder(QString &src, QString &newName);
    void hideOrUnhideFile(QString &src, bool hide);

};

#endif // MAINWINDOW_H
