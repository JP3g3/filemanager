#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QObject>
#include "mainwindow.h"

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(int _id, QString text, QWidget *parent = 0);
    QString getName();
    ~Dialog();

public slots:
    void on_buttonBox_accepted();

private:
    Ui::Dialog *ui;
    QString name;
    int id;
};

#endif // DIALOG_H
