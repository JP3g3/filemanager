#include "login.h"
#include "ui_login.h"

login::login(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::login)
{
    ui->setupUi(this);
    host = "";
    loginName = "";
    password = "";
}

login::~login()
{
    delete ui;
}


QString login::getHost(){
    return host;
}

QString login::getLogin(){
    return loginName;
}

QString login::getPassword(){
    return password;
}

void login::on_buttonBox_accepted()
{
    host = ui->le_host->text();
    loginName = ui->le_login->text();
    password = ui->le_password->text();
}
