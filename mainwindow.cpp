#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->openFile, SIGNAL(triggered()), this, SLOT(openFile()));
    connect(ui->exit, SIGNAL(triggered()), this, SLOT(exit()));
    connect(ui->changeName, SIGNAL(triggered()), this, SLOT(changeName()));
    connect(ui->deleteFileOrFolder, SIGNAL(triggered()), this, SLOT(deleteFileOrFolder()));
    connect(ui->hide, SIGNAL(triggered()), this, SLOT(hideFile()));
    connect(ui->unhide, SIGNAL(triggered()), this, SLOT(unhideFile()));
    connect(ui->createFile, SIGNAL(triggered()), this, SLOT(createFile()));
    connect(ui->createFolder, SIGNAL(triggered()), this, SLOT(createFolder()));
    connect(ui->connect, SIGNAL(triggered()), this, SLOT(connectToServer()));
    connect(ui->disconnect, SIGNAL(triggered()), this, SLOT(disconnectServer()));

    QString path = "";
    fileModelLeft = new QFileSystemModel(this);
    fileModelRight = new QFileSystemModel(this);
    fileModelLeft->setRootPath(path);
    fileModelRight->setRootPath(path);
    stackOfPathsLeft.push(path);
    stackOfPathsRight.push(path);

    ui->lv_left->setModel(fileModelLeft);
    ui->lv_right->setModel(fileModelRight);
    ui->le_left->setText(path);
    ui->le_right->setText(path);

    pathLeft = "";
    pathRight = "";
    hostName = "";
    userName = "";
    password ="";
}

MainWindow::~MainWindow()
{
    delete ui;
}


// ------------------------  Private slots -------------------------------

void MainWindow::on_lv_left_doubleClicked(const QModelIndex &index)
{
    pathLeft = "";
    QString path = fileModelLeft->fileInfo(index).absoluteFilePath();
    ui->lv_left->setRootIndex(fileModelLeft->setRootPath(path));
    ui->le_left->setText(path);
    stackOfPathsLeft.push(path);

    if (stackOfPathsLeft.count() > 1)
        ui->pb_arrowup_left->setEnabled(true);
}

void MainWindow::on_lv_right_doubleClicked(const QModelIndex &index)
{
    pathRight = "";
    QString path = fileModelRight->fileInfo(index).absoluteFilePath();
    ui->lv_right->setRootIndex(fileModelRight->setRootPath(path));
    ui->le_right->setText(path);
    stackOfPathsRight.push(path);

    if (stackOfPathsRight.count() > 1)
        ui->pb_arrowup_right->setEnabled(true);
}

void MainWindow::on_lv_left_clicked(const QModelIndex &index)
{
    pathLeft = fileModelLeft->fileInfo(index).absoluteFilePath();
}

void MainWindow::on_lv_right_clicked(const QModelIndex &index)
{
    pathRight = fileModelRight->fileInfo(index).absoluteFilePath();
}

void MainWindow::on_pb_arrowup_left_clicked()
{
    if (stackOfPathsLeft.count() > 1) {
        stackOfPathsLeft.pop();
        QString path = stackOfPathsLeft.pop();
        ui->lv_left->setRootIndex(fileModelLeft->setRootPath(path));
        ui->le_left->setText(path);
        stackOfPathsLeft.push(path);

        if (stackOfPathsLeft.count() < 2)
            ui->pb_arrowup_left->setEnabled(false);
    }
}

void MainWindow::on_pb_arrowup_right_clicked()
{
    if (stackOfPathsRight.count() > 1) {
        stackOfPathsRight.pop();
        QString path = stackOfPathsRight.pop();
        ui->lv_right->setRootIndex(fileModelRight->setRootPath(path));
        ui->le_right->setText(path);
        stackOfPathsRight.push(path);

        if (stackOfPathsRight.count() < 2)
            ui->pb_arrowup_right->setEnabled(false);
    }
}

void MainWindow::on_pb_move_right_clicked()
{
    moveFileAndFolder(pathLeft, pathRight);
}

void MainWindow::on_pb_move_left_clicked()
{
    moveFileAndFolder(pathRight, pathLeft);
}

void MainWindow::on_pb_copy_right_clicked()
{
    copyFileAndFolder(pathLeft, pathRight);
}

void MainWindow::on_pb_copy_left_clicked()
{
    copyFileAndFolder(pathRight, pathLeft);
}

void MainWindow::openFile(){
    if ((ui->le_left->isActiveWindow() == true) && (pathLeft != "")){
        QFileInfo fileInfo(pathLeft);
        if (fileInfo.isDir() == false){
            File f(pathLeft, this);
            f.exec();
        }
        else {
            Alert a("Nie można otworzyć folderu", this);
            a.exec();
        }
    }

    else if ((ui->le_right->isActiveWindow() == true) && (pathRight != "")){
        QFileInfo fileInfo(pathRight);
        if (fileInfo.isDir() == false){
            File f(pathRight, this);
            f.exec();
        }
        else {
            Alert a("Nie można otworzyć folderu", this);
            a.exec();
        }
    }
}

void MainWindow::exit(){
    MainWindow::close();
}

void MainWindow::changeName(){
    Dialog d (3, "Podaj nową nazwę: ", this);
    d.exec();

    QString name;
    if (d.result() == QDialog::Accepted) {
        name = d.getName();
        if ((ui->le_left->isActiveWindow() == true) && (pathLeft != "")) {
            QFileInfo fileInfo(pathLeft);
            if (fileInfo.isDir() == true)
                renameFolder(pathLeft, name);
            else
                renameFile(pathLeft, name);

        }
        if ((ui->le_right->isActiveWindow() == true) && (pathRight != "")) {
            QFileInfo fileInfo(pathRight);
            if (fileInfo.isDir() == true)
                renameFolder(pathRight, name);
            else
                renameFile(pathRight, name);
        }
    }
}

void MainWindow::deleteFileOrFolder() {

    QMessageBox::StandardButton przycisk;
    przycisk = QMessageBox::question(this, "Usuwanie", "Czy jesteś pewny?", QMessageBox::Yes | QMessageBox::No);

    if (przycisk == QMessageBox::Yes){
        if ((ui->le_left->isActiveWindow() == true) && (pathLeft != "")) {
            QFileInfo fileInfo(pathLeft);
            if (fileInfo.isDir() == true)
                deleteFolder(pathLeft);
            else
                deleteFile(pathLeft);
        }
        if ((ui->le_right->isActiveWindow() == true) && (pathRight != "")) {
            QFileInfo fileInfo(pathRight);
            if (fileInfo.isDir() == true)
                deleteFolder(pathRight);
            else
                deleteFile(pathRight);
        }
    }
}

void MainWindow::hideFile(){
    if ((ui->le_left->isActiveWindow() == true) && (pathLeft != ""))
        hideOrUnhideFile(pathLeft, true);

    else if ((ui->le_right->isActiveWindow() == true) && (pathRight != ""))
        hideOrUnhideFile(pathRight, true);

}

void MainWindow::unhideFile(){
    if ((ui->le_left->isActiveWindow() == true) && (pathLeft != ""))
        hideOrUnhideFile(pathLeft, false);

    else if ((ui->le_right->isActiveWindow() == true) && (pathRight != ""))
        hideOrUnhideFile(pathRight, false);

}

void MainWindow::createFile(){
    Dialog d (2, "Podaj nazwe pliku:", this);
    d.exec();

    QString name;
    if (d.result() == QDialog::Accepted) {
        name = d.getName();
        if (name != "") {
            if ((ui->le_left->isActiveWindow() == true) || (ui->le_right->isActiveWindow() == true)) {
                QString path;
                if (ui->le_left->isActiveWindow() == true) {
                    path = stackOfPathsLeft.pop();
                    stackOfPathsLeft.push(path);
                }
                else {
                    path = stackOfPathsRight.pop();
                    stackOfPathsRight.push(path);
                 }
                path += "/" + name;
                QFile file(path);
                if (!file.exists())
                    file.open(QIODevice::ReadWrite);
                else {
                    Alert a ("Plik juz istnieje", this);
                    a.exec();
                }
            }
        }
    }
}

void MainWindow::createFolder(){

    Dialog d (1, "Podaj nazwe folderu:", this);
    d.exec();

    QString name;
    if (d.result() == QDialog::Accepted) {
        name = d.getName();
        if (name != "") {
            if ((ui->le_left->isActiveWindow() == true) || (ui->le_right->isActiveWindow() == true)) {
                QString path;
                if (ui->le_left->isActiveWindow() == true) {
                    path = stackOfPathsLeft.pop();
                    stackOfPathsLeft.push(path);
                }
                else {
                    path = stackOfPathsRight.pop();
                    stackOfPathsRight.push(path);
                 }
                path += "/" + name;
                QDir dir(path);
                if (!dir.exists())
                    QDir().mkdir(path);
                else {
                    Alert a ("Folder juz istnieje", this);
                    a.exec();
                }
            }

        }
    }
}

void MainWindow::connectToServer(){
    /*login l (this);
    l.exec();

    if (l.result() == QDialog::Accepted) {*/
        hostName = "mkwk018.cba.pl";//"127.0.0.1";//l.getHost();
        userName = "mobilki";//"test";//l.getLogin();
        password = "Mobilki123";"123";//l.getPassword();

        if (hostName != "" && userName != "" && password != ""){
            QUrl url("ftp://"+hostName);
            url.setUserName(userName);
            url.setPassword(password);

            //ui->le_left->setText(url.path());
            //ui->lv_left->setRootIndex(fileModelLeft->setRootPath(url.path()));
        }
    //}
}

void MainWindow::disconnectServer(){

}

// ------------------------  Private function -------------------------------

void MainWindow::copyFileAndFolder(QString &src, QString &dst){
    if (src != "" && dst != "") {
        QFileInfo fileInfo1(src);
        QFileInfo fileInfo2(dst);

        if (fileInfo2.isDir() == true) {
            QString temp = dst + "/" + QFileInfo(src).fileName();
            if (fileInfo1.isFile() == true) {
                if (QFile::copy(src, temp) == true) {
                    Alert a ("Skopiowano plik", this);
                    a.exec();
                }
            }
            else {
                if (copyFolder(src, temp) == true) {
                    Alert a("Skopiowano folder", this);
                    a.exec();
                }
            }
        }
        else {
            Alert a("Zaznacz poprawnie sciezke", this);
            a.exec();
        }
    }
    else {
        Alert a("Zaznacz sciezke", this);
        a.exec();
    }
}

bool MainWindow::copyFolder(const QString &src, const QString &dst){
    QDir dir(src);
    if (!dir.exists())
        return false;

    QDir dir2(dst);
    if (!dir2.exists())
        QDir().mkdir(dst);
    else return false;

    foreach (QString d, dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot)) {
        QString dst_path = dst + QDir::separator() + d;
        dir.mkpath(dst_path);
        copyFolder(src+ QDir::separator() + d, dst_path);
    }

    foreach (QString f, dir.entryList(QDir::Files)) {
        QFile::copy(src + QDir::separator() + f, dst + QDir::separator() + f);
    }
    return true;
}

void MainWindow::moveFileAndFolder(QString &src, QString &dst){
    if (src != "" && dst != "") {
        QFileInfo fileInfo1(src);
        QFileInfo fileInfo2(dst);

        if (fileInfo2.isDir() == true) {
            QDir dir;
            QString temp = dst + "/" + QFileInfo(src).fileName();
            if (!dir.rename(src, temp)){
                Alert a ("Nie udalo się", this);
                a.exec();
            }
            else {
                if (fileInfo1.isFile() == true) {
                    Alert a ("Folder zostal przeniesiony", this);
                    a.exec();
                }
                else {
                    Alert a ("Plik zostal przeniesiony", this);
                    a.exec();
                }
            }
        }
        else {
            Alert a("Zaznacz poprawnie sciezke", this);
            a.exec();
        }
    }
    else {
        Alert a("Zaznacz sciezke", this);
        a.exec();
    }
}

void MainWindow::deleteFile(QString &src) {
    QFile file (src);
    if (file.exists()){
        if (file.remove() == true) {
            Alert a ("Udało się usunąć plik", this);
            a.exec();
        }
        else {
            Alert a ("Nie udało się usunąć pliku", this);
            a.exec();
        }
    }
}

void MainWindow::deleteFolder(QString &src) {
    QDir dir (src);
    if (dir.exists()){
        if (dir.removeRecursively() == true) {
            Alert a ("Udało się usunąć folder", this);
            a.exec();
        }
        else {
            Alert a ("Nie udało się usunąć folderu", this);
            a.exec();
        }
    }
}

void MainWindow::renameFile(QString &src, QString &newName){
    QFile file (src);
    if (file.exists()){
        QFileInfo file(src);
        if (QFile::rename(src, (file.absoluteDir().absolutePath() + "/" + newName)) == true) {
            Alert a ("Udało się zmienić nazwę pliku", this);
            a.exec();
        }
        else {
            Alert a ("Nie udało się zmienić nazwę pliku", this);
            a.exec();
        }
    }
}

void MainWindow::renameFolder(QString &src, QString &newName){
    QDir dir (src);
    if (dir.exists()){
        QFileInfo file(src);
        QStringList l = src.split("/");
        QString poprzednia = "";
        for (int i = 0; i < l.length() - 1; i++) poprzednia += l[i] + "/";
        if (QDir().rename(src, (poprzednia + newName)) == true) {
            Alert a ("Udało się zmienić nazwę folderu", this);
            a.exec();
        }
        else {
            Alert a ("Nie udało się zmienić nazwę folderu", this);
            a.exec();
        }
    }
}

void MainWindow::hideOrUnhideFile(QString &src, bool hide){
    wchar_t* fileLPCWSTR = new wchar_t[src.length() + 1];
    src.toWCharArray(fileLPCWSTR);
    fileLPCWSTR[src.length()] = 0;

    int attr = GetFileAttributes(fileLPCWSTR);
    if (hide == false)
        SetFileAttributes(fileLPCWSTR, attr & ~FILE_ATTRIBUTE_HIDDEN);
    else SetFileAttributes(fileLPCWSTR, attr | FILE_ATTRIBUTE_HIDDEN);
    delete[] fileLPCWSTR;
}

