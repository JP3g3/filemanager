#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(int _id, QString text, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    id = _id;
    name = "";
    ui->setupUi(this);
    ui->label->setText(text);

    QRegExp re("^[A-Za-z0-9._-()ąęćżźółĄŚĆŻÓŁĘŃń!$]*$");
    QRegExpValidator *validator = new QRegExpValidator(re, this);
    ui->lineEdit->setValidator(validator);
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_buttonBox_accepted() {
    name = ui->lineEdit->text();
}

QString Dialog::getName() {
    return name;
}
